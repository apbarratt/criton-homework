import { Component } from '@angular/core';
import { Http, Response } from '@angular/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  error = '';
  address = '';
  latitude;
  longitude;
  loading = false;
  apiKey = 'AIzaSyDUMJPKpwVKTOibfs0RERo_KdoWP4W9WVQ';

  constructor(private http: Http) {}

  onSubmit() {
    this.loading = true;
    this.error = '';
    this.latitude = '';
    this.longitude = '';
    let address = encodeURI(this.address);
    this.http.get(`https://maps.googleapis.com/maps/api/geocode/json?key=${this.apiKey}&address=${address}`)
    .subscribe(
      response => {
        let results = response.json().results;
        if(results.length) {
          this.latitude = results[0].geometry.location.lat;
          this.longitude = results[0].geometry.location.lng;
        } else {
          this.error = "No results found.";
        }
      },
      error => {
        let errorResponse = error.json();
        switch(errorResponse.status) {
          case 'INVALID_REQUEST':
            this.error = 'Yeah, looks like that wasn\'t an address... try again maybe?';
            break;
          default:
            this.error = `Unknown error from api: ${errorResponse.error_message}`;
            break;
        }
      }
    )
  }
}
